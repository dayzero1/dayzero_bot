FROM python:3

ADD main.py /
ADD .env /


RUN pip install -U discord.py
RUN pip install -U python-dotenv

CMD [ "python", "./main.py"]
